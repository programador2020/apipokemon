class Pokemones {
    static getPokemones() {
        fetch("https://pokeapi.co/api/v2/pokemon?limit=10")
            .then(response => response.json())
            .then(datos => {
                let template =
                    datos.results.map(dato =>
                        this.fetchPokemonData(dato.url)
                    )
            }
            )
    }

    static fetchPokemonData(url) {
        fetch(url)
            .then(response => response.json())
            .then(pokemon => this.renderPokemon(pokemon))
    }

    static renderPokemon(pokemon) {
        let template =
            `
                <article>
                    <h3>${this.capitalizeFirstLetter(pokemon.name)}</h3>
                    <div>Types: <span> ${pokemon.types.map(element => element.type.name).join(' | ')} </span> </div>
                    <img src="https://pokeres.bastionbot.org/images/pokemon/${pokemon.id}.png" />                     
                </article>
            `
        let template_before = document.querySelector("#listado").innerHTML;
        template = template_before + template;
        document.querySelector("#listado").innerHTML = template;


    }

    static capitalizeFirstLetter(name) {
        return name.charAt(0).toUpperCase() + name.slice(1);
    }

    static main() {
        this.getPokemones();
    }
}

Pokemones.main();